package dam.android.miguelangel.u3t3menuofactivities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class ItemDetailActivity extends AppCompatActivity {

    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_detail);

        setUI();

    }

    private void setUI(){

        textView = findViewById(R.id.textView4);
        Intent i = getIntent();
        String activity = i.getStringExtra("activityName");
        textView.setText("Hello I'm " + activity);

    }
}