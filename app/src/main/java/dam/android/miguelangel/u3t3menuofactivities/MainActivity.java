package dam.android.miguelangel.u3t3menuofactivities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements MyAdapter.OnItemClickListener{

    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private Button insert;
    private Button delete;
    private Button restore;
    private ImageView vacio;

    private String[] myDataset = {"Activity1", "Activity2", "Activity3"};
    List<Item> items;
    List<Item> original = new ArrayList<Item>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();
    }

    private void setUI() {

        insert = findViewById(R.id.insert);
        delete = findViewById(R.id.delete);
        restore = findViewById(R.id.restore);
        vacio = findViewById(R.id.vacio);
        vacio.setImageResource(R.drawable.vacio);
        vacio.setVisibility(View.INVISIBLE);

        recyclerView = findViewById(R.id.recyclerViewActivities);

        items = new ArrayList<Item>();

        items.add(new Item("Android 11", "30", "11.0", R.drawable.a11));
        items.add(new Item("Android 10", "29", "10.0", R.drawable.a10));
        items.add(new Item("Pie", "28", "9.0", R.drawable.pie));
        items.add(new Item("Oreo", "26-27", "8.0-8.1", R.drawable.oreo));
        items.add(new Item("Nougat", "24-25", "7.0-7.1.2", R.drawable.nougat));
        items.add(new Item("Marshmallow", "23", "6.0-6.0.1", R.drawable.marshmallow));
        items.add(new Item("Lollipop", "21-22", "5.0-5.1.1", R.drawable.lollipop));

        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

        mAdapter = new MyAdapter(items, this);
        recyclerView.setAdapter(mAdapter);

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);

    }

    @Override
    public void onItemClick(String activityName) {
        Toast.makeText(this, activityName, Toast.LENGTH_LONG).show();
        Intent i = new Intent(this, ItemDetailActivity.class);
        String activity = activityName;
        i.putExtra("activityName", activity);
        startActivity(i);
    }

    public void insertAndroid12(View view) {

        guardar();
        items.add(new Item("Android12", "31", "12.0", R.drawable.a11));
        vacio.setVisibility(View.INVISIBLE);
        refrescar();

    }

    private void guardar() {

        for (int i = 0; i < items.size(); i++){

            original.add(items.get(i));

        }

    }

    public void deleteAll(View view) {

        guardar();
        items.removeAll(items);
        refrescar();
        vacio.setVisibility(View.VISIBLE);

    }


    public void restoreList(View view) {

        if (items != null) {
            items.removeAll(items);
        }
        vacio.setVisibility(View.INVISIBLE);

        for (int i = 0; i < original.size(); i++) {

            items.add(original.get(i));

        }

        refrescar();

    }

    private void refrescar(){

        mAdapter = new MyAdapter(items, this);
        recyclerView.setAdapter(mAdapter);

    }

    ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {

            int position = viewHolder.getAdapterPosition();

            switch (direction) {
                case ItemTouchHelper.LEFT:
                    break;
                case ItemTouchHelper.RIGHT:
                    items.remove(position);
                    if (items.size() <= 0) {
                        vacio.setVisibility(View.VISIBLE);
                    }
                    refrescar();
                    break;
            }

        }
    };

}