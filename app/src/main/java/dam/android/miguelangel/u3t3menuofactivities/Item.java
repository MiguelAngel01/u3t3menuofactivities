package dam.android.miguelangel.u3t3menuofactivities;

import android.widget.ImageView;

public class Item {

    private String nombre;
    private String api;
    private String version;
    private int image;

    public Item(String nombre, String api, String version, int image){
        this.nombre = nombre;
        this.api = api;
        this.version = version;
        this.image = image;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApi() {
        return api;
    }

    public void setApi(String api) {
        this.api = api;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
