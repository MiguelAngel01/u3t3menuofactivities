package dam.android.miguelangel.u3t3menuofactivities;

import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    public interface  OnItemClickListener {
        void onItemClick(String activityName);
    }

    private List<Item> items;

    private OnItemClickListener listener;

    static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        TextView version;
        TextView api;
        ImageView image;

        MyViewHolder(View itemView) {

            super(itemView);
            name = itemView.findViewById(R.id.name);
            version = itemView.findViewById(R.id.version);
            api = itemView.findViewById(R.id.api);
            image = itemView.findViewById(R.id.image);

        }

        public void bind(String activityName, OnItemClickListener listener) {

            this.itemView.setOnClickListener(v -> listener.onItemClick(name.getText().toString()));

        }

    }

    MyAdapter(List<Item> items, OnItemClickListener listener) {

        this.items = items;
        this.listener = listener;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview, parent, false);
        return new MyViewHolder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position) {
        viewHolder.name.setText(items.get(position).getNombre());
        viewHolder.api.setText(items.get(position).getApi());
        viewHolder.version.setText(items.get(position).getVersion());
        viewHolder.image.setImageResource(items.get(position).getImage());
    }

    @Override
    public int getItemCount() {

        return items.size();

    }

}
